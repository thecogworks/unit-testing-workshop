﻿module.exports = function (config) {
    config.set({
        basePath: "../../Source/UnitTestingWorkshop.Web",
        files: [
            "umbraco/lib/angular/1.1.5/angular.js",
            "umbraco/lib/angular/1.1.5/angular-mocks.js",
            "umbraco/lib/jquery/jquery.min.js",
            "App_Plugins/forum/forum.module.js",
            "App_Plugins/forum/**/*.js",
            "App_Plugins/forum/**/*.html",
            "../../Tests/UnitTestingWorkshop.Client.Test/Exercise7/**/*.js"
        ],
        preprocessors: {
            "App_Plugins/forum/**/*.html": ["ng-html2js"]
        },
        frameworks: [
            "jasmine"
        ],
        reporters: [
            "progress",
            "kjhtml"
        ],
        browsers: [
            "Chrome"
        ],
        client: {
            clearContext: false
        },
        ngHtml2JsPreprocessor: {
            prependPrefix: "/",
            moduleName: "forum"
        }
    });
}