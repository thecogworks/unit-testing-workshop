﻿/* global describe */
(function () {

    describe("comment count directive", function () {

        var compile,
            scope,
            httpBackend,
            element;

        // We need to use the $provide registration with module to stub editorState for the controller
        beforeEach(module("forum", function($provide) {
            $provide.value("editorState", { current: { id: 1 } });
        }));

        // This time we use $compile to compile HTML instead of creating a controller
        beforeEach(inject(function($compile, $rootScope, $httpBackend) {
            compile = $compile;
            scope = $rootScope.$new();
            httpBackend = $httpBackend;

            // Commonly used result
            httpBackend.expectGET("/umbraco/backoffice/api/commentcount/1").respond(200, { count: 5 });

            // Create the element and make it exercise the bindings
            element = compile("<comment-count/>")(scope);
            scope.$digest();
        }));

        it("shows a loading indicator while waiting for count", function() {

            expect(element.text()).toMatch(/Loading.../);

        });

        xit("shows amount of posts when done loading", function() {

            // TODO: Flush like for the controller

            expect(element.text()).toMatch(/There are 5 posts/);

        });

        xit("highlights when there are posts", function() {

            // same as above, new "fact"

            expect(element.hasClass("has-posts")).toBe(true);

        });

        xit("dims when there are no posts", function () {

            // TODO: Reset httpBackend to provide new case

            expect(element.hasClass("has-no-posts")).toBe(true);

        });
    });

}());
