﻿(function () {

    describe("comment count controller", function () {

        var scope,
            controllerFactory;

        // Factory method so we can provide stubs to the controller
        function createController() {
            controllerFactory("comment.count.controller", {
                $scope: scope
            });
        }

        // We are testing the forum module
        beforeEach(module("forum"));

        // Inject the angular servies and initialize state
        beforeEach(inject(function ($rootScope, $controller) {

            // Create a new scope for the controller
            scope = $rootScope.$new();
            scope.model = { value: "" }; // Typical property editor state, (we're not going to use it here)

            // Add reference to $controller (Factory)
            controllerFactory = $controller;

            // TODO: Add reference to $httpBackend

        }));

        it("sets count to N/A while loading", function () {

            // Create the controller
            createController();

            // Expect count
            expect(scope.count).toBe("N/A");

        });

        xit("gets count for current content from server", function () {

            var expectedCount = 5;

            // Stub a current content on editorState.current
            // Provide it to the controller

            // Set expectation on $httpBackend using $httpBackend.expectGET("urn").respond(200, resultValue);
            // TODO: Setup httpBackend

            createController();

            // Flush http using $httpBackend.flush();
            // TODO: flush

            // Expect scope to be stubbed value
            expect(scope.count).toBe(expectedCount);

        });

    });

}());