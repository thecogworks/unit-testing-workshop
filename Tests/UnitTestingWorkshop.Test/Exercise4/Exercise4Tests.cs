﻿using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using Umbraco.Core.Models;
using Umbraco.Web;
using UnitTestingWorkshop.BusinessLogic.Services;
using UnitTestingWorkshop.BusinessLogic.Wrappers;

namespace UnitTestingWorkshop.Test.Exercise4
{
    [TestFixture]
    public class When_Getting_Featured_Forum_Topics
    {
        WidgetService _widgetService;

        private Mock<UmbracoHelper> _umbracoHelperMock;

        #region Uncomment to see answer
        // UNCOMMENT ME!
        // private Mock<IUmbracoHelperWrapper> _umbracoHelperMock;
        #endregion


        [SetUp]
        public void Setup()
        {
            _umbracoHelperMock = new Mock<UmbracoHelper>();
            _widgetService = new WidgetService(_umbracoHelperMock.Object);


            #region Uncomment to see answer for Task 1
            // UNCOMMENT ME!
            //_umbracoHelperMock = new Mock<IUmbracoHelperWrapper>();
            // _widgetService = new WidgetService(_umbracoHelperMock.Object);
            #endregion
        }


        [Test]
        public void Selected_Content_Is_Listed()
        {
            var containerNodeMock = new Mock<IPublishedContent>();

            var nodes = new List<IPublishedContent>() { new Mock<IPublishedContent>().Object };
            containerNodeMock.Setup(x => x.Children).Returns(nodes);

            _umbracoHelperMock.Setup(x => x.TypedContent(1234)).Returns(containerNodeMock.Object);
            var res = _widgetService.GetPromotedContent();

            Assert.IsNotEmpty(res);
        }




        [Test]
        public void When_No_Selected_Content_Exists_Then_Show_Nothing()
        {


            #region Uncomment to see answer for task 2

            // UNCOMMENT ME!
            //_umbracoHelperMock.Setup(x => x.TypedContent(1234)).Returns((IPublishedContent)null);
            //var res = _widgetService.GetPromotedContent();

            //Assert.IsEmpty(res);
            #endregion

        }
    }
}
