﻿using Moq;
using NUnit.Framework;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using UnitTestingWorkshop.BusinessLogic.Services;

namespace UnitTestingWorkshop.Test.Exercise3
{
    [TestFixture]
    public class When_Creating_Forum_Topic
    {
        PostService _postService;
        Mock<IContentService> _contentServiceMock;
        private Mock<IContent> _contentMock;

        const int MemberId = 123;
        const int ParentNodeId = 999;
        const string TopicTitle = "some topic title";
        const string TopicBody = "some topic body";


        [SetUp]
        public void Setup()
        {
             _contentMock = new Mock<IContent>();

            _contentServiceMock = new Mock<IContentService>();
            _postService = new PostService(_contentServiceMock.Object);

        }



        /// <summary>
        /// Note that this test is kind of an evolutionary step toward getting to the next test.
        /// 
        /// You would not normally keep this test around, as it is too simple, and the result value is 
        /// irrelevant to the logic in the actual test.
        /// </summary>
        [Test]
        public void Valid_Topics_Are_Accepted()
        {
            // set the fake ContentService to return my content mock, when given these specific parameters 
            _contentServiceMock.Setup(x => x.CreateContent(TopicTitle, ParentNodeId, "Post", 0)).Returns(_contentMock.Object);

            var result = _postService.CreateTopic(TopicTitle, TopicBody, ParentNodeId, MemberId);

            // assert that the result was "success"
            Assert.AreEqual("success", result);
        }


        [Test]
        public void Valid_Topics_Are_Persisted()
        {
            // set the fake ContentService to return my content mock, when given these specific parameters 
            _contentServiceMock.Setup(x => x.CreateContent(TopicTitle, ParentNodeId, "Post", 0)).Returns(_contentMock.Object);

            var result = _postService.CreateTopic(TopicTitle, TopicBody, ParentNodeId, MemberId);

            // assert that the result was "success"
            Assert.AreEqual("success", result);



            #region Uncomment to see answer for Task 2
            // UNCOMMENT ME!
            //// verify that memberId was set
            //_contentMock.Verify(x => x.SetValue("memberId", MemberId));

            //// verify that the methods Save and Publish were called
            // _contentServiceMock.Verify(x => x.CreateContent(TopicTitle, ParentNodeId, "Post", 0));
            //_contentServiceMock.Verify(x => x.Save(_contentMock.Object, 0, true));
            //_contentServiceMock.Verify(x => x.Publish(_contentMock.Object, 0));
            #endregion
        }


        [Test]
        public void Invalid_Topics_Are_Not_Persisted()
        {


            #region Uncomment to see answer for Task 3
            // UNCOMMENT ME!
            //var result = _postService.CreateTopic(string.Empty, TopicBody, ParentNodeId, MemberId);

            //// assert that the result was "success"
            //Assert.AreEqual("Invalid title", result);

            //// verify that the methods Save and Publish were NEVER called
            //_contentServiceMock.Verify(x => x.Save(_contentMock.Object, 0, true), Times.Never());
            //_contentServiceMock.Verify(x => x.Publish(_contentMock.Object, 0), Times.Never());
            #endregion
        }
        
    }
}
