﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using Moq;
using NUnit.Framework;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Tests.TestHelpers;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using Umbraco.Web.Routing;
using UnitTestingWorkshop.BusinessLogic.Services;
using UnitTestingWorkshop.Web.Controllers.Forms;
using UnitTestingWorkshop.Web.Models;

namespace UnitTestingWorkshop.Test.Exercise5
{
    [TestFixture]
    public class When_Creating_Member
    #region Uncomment for 6.2, delete for 6.3
    // : BaseRoutingTest
    #endregion
    {
        #region Uncomment for 6.2, delete for 6.3
        //private RouteData routeData;

        //[SetUp]
        //public void Setup()
        //{
        //    var currentPage = Mock.Of<IPublishedContent>();
        //    var settings = SettingsForTests.GenerateMockSettings();
        //    routeData = new RouteData();
        //    var uri = new Uri("http://localhost");
        //    var routingContext = GetRoutingContext(uri.ToString(), -1, routeData, true, settings);
        //    var publishedContentRequest = new PublishedContentRequest(uri, routingContext, settings.WebRouting, (s) => null);
        //    publishedContentRequest.PublishedContent = ;
        //    routeData.DataTokens.Add("umbraco-route-def", new RouteDefinition { PublishedContentRequest = publishedContentRequest });
        //}
        #endregion

        #region Uncomment for 6.3
        //private UmbracoSupport support = new UmbracoSupport();

        //[SetUp]
        //public void Setup()
        //{
        //    support.SetupUmbraco();
        //}

        //[TearDown]
        //public void TearDown()
        //{
        //    support.DisposeUmbraco();
        //}
        #endregion

        [Test]
        public void With_Valid_Registration_Then_Redirects_To_Current_Page()
        {
            var memberService = Mock.Of<IMemberService>();
            var accountService = new AccountService(memberService);
            var controller = new AccountController(accountService);

            #region Uncomment for 6.2, delete for 6.3
            //controller.ControllerContext = new ControllerContext {RouteData = routeData};
            #endregion

            #region Uncomment for 6.3
            //support.PrepareController(controller);
            #endregion

            var result = controller.CreateMember(new CreateMemberModel
            {
                Name = "user",
                Email = "user@somewhere.com",
                Password = "pwd"
            });

            Assert.That(result, Is.InstanceOf<RedirectToUmbracoPageResult>());
        }
    }
}
