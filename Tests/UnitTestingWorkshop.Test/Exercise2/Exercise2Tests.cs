﻿using NUnit.Framework;
using UnitTestingWorkshop.BusinessLogic.Services;
using UnitTestingWorkshop.Test.Exercise2.Fakes;

namespace UnitTestingWorkshop.Test.Exercise2
{
    [TestFixture]
    public class When_Creating_A_Member
    {
        AccountService _accountService;
        private FakeMemberService _fakeMemberService;

        [SetUp]
        public void Setup()
        {
            _fakeMemberService = new FakeMemberService();

            _accountService = new AccountService(_fakeMemberService);
        }




        [Test]
        public void For_New_Member_Then_Member_Is_Created()
        {
            #region Uncomment to see answer for Task 3
            // UNCOMMENT ME! - Task 3
            //_fakeMemberService.MemberExists = false;
            #endregion

            var result = _accountService.Create("SomeName", "name@company.com", "password");

            Assert.AreEqual("success", result);
        }


        [Test]
        public void For_Existing_Member_Show_Member_Exists()
        {
            #region Uncomment to see answer for Task 3
            // UNCOMMENT ME! - Task 3
            //_fakeMemberService.MemberExists = true;
            #endregion

            var result = _accountService.Create("SomeName", "name@company.com", "password");

            Assert.AreEqual("exists", result);
        }
        
    }
}
