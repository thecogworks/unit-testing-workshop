﻿using NUnit.Framework;
using System.Web;
using UnitTestingWorkshop.BusinessLogic.Services;

namespace UnitTestingWorkshop.Test.Exercise1
{
    [TestFixture]
    public class When_Rendering_UI
    {
        private SettingsService _settingsService;
        [SetUp]
        public void Setup()
        {
            _settingsService = new SettingsService();
        }


        [Test]
        public void Cookieless_User_Is_Shown_The_Simple_UI()
        {
            var res = _settingsService.IsSimpleUI();

            Assert.True(res);
        }



        
        /* Uncomment me - Ex 1.2
        [Test]
        public void Opted_In_User_Is_Shown_The_Complex_UI()
        {
            // setup
            var cookies = new HttpCookieCollection();

            var cookie = new HttpCookie("Forum");
            cookie.Values.Add("ComplexUI", "1");
            cookies.Add(cookie);

            // run the test          
            var res = _settingsService.IsSimpleUI(cookies);

            Assert.True(res);
        }
        */



        [Test]
        public void Opted_Out_User_Is_Shown_The_Simple_UI()
        {
           // FINISH ME
        } 
        
    }
}
