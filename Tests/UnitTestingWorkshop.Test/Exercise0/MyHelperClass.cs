﻿using System;

namespace UnitTestingWorkshop.Test.Exercise0
{
    public class MyHelperClass
    {
        public bool GetBoolResult(string s)
        {
            if (s == "1")
            {
                return true;
            }
            if (s == "0")
            {
                return true;
            }

            #region Uncomment to see answer
          //   throw new Exception("Invalid input: " + s);
            #endregion

            return false;
        }
    }
}