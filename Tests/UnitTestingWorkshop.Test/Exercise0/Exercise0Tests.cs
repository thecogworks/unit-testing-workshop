﻿using System;
using NUnit.Framework;

namespace UnitTestingWorkshop.Test.Exercise0
{
    [TestFixture]
    public class Exercise0Tests
    {

        [SetUp]
        public void Setup()
        {
            // this is run automatically each time a test executes
        }


        [Test]
        public void When_Input_Is_1_Then_Result_Is_True()
        {
            var helper = new MyHelperClass();
            var result = helper.GetBoolResult("1");

            Assert.True(result);

            // note: if you are comparing strings, you would do something like this Assert.AreEqual("The result", result);
        }


        [Test]
        public void When_Input_Is_0_Then_Result_Is_False()
        {
            var helper = new MyHelperClass();
            var result = helper.GetBoolResult("0");

            Assert.False(result);
        }


        [Test]
        public void When_Input_Is_Invalid_Then_Throw_Exception()
        {
            var helper = new MyHelperClass();

            Assert.Throws<Exception>(() => helper.GetBoolResult("SiteCore"));
        }
    }
}
