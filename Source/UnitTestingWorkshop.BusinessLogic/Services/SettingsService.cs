﻿using System.Web;

namespace UnitTestingWorkshop.BusinessLogic.Services
{
    public interface ISettingsService
    {
        bool IsSimpleUI();
    }


    public class SettingsService : ISettingsService
    {


        /// <summary>
        /// Returns if the forum view is in simple mode.
        /// </summary>
        public bool IsSimpleUI()
        {
            var cookies = HttpContext.Current.Request.Cookies;

            var cookie = cookies["Forum"];

            if (cookie == null)
            {
                return true;
            }

            if (cookie.Values["ComplexUI"] == "1")
            {
                return true;
            }

            return false;
        }


        #region Uncomment to see answer
        ///// <summary>
        ///// Returns if the forum view is in simple mode.
        ///// </summary>
        //public bool IsSimpleUI(HttpCookieCollection cookies)
        //{
        //    //var cookies = HttpContext.Current.Request.Cookies;

        //    var cookie = cookies["Forum"];

        //    if (cookie == null)
        //    {
        //        return true;
        //    }

        //    if (cookie.Values["ComplexUI"] == "1")
        //    {
        //        return true;
        //    }

        //    return false;
        //}
        #endregion
    }
}
