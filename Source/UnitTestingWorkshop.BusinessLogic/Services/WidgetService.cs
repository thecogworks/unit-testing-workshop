﻿using System.Collections.Generic;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace UnitTestingWorkshop.BusinessLogic.Services
{
    public interface IWidgetService
    {
        IEnumerable<IPublishedContent> GetPromotedContent();
    }


    public class WidgetService : IWidgetService
    {
        private UmbracoHelper _umbracoHelper;

        #region Uncomment to see answer
        //private IUmbracoHelperWrapper _umbracoHelper;
        #endregion

        public WidgetService(UmbracoHelper umbracoHelper)
        {
            _umbracoHelper = umbracoHelper;
        }


        #region Uncomment to see answer
        //public WidgetService(IUmbracoHelperWrapper umbracoHelper)
        //{
        //    _umbracoHelper = umbracoHelper;
        //}
        #endregion


        public IEnumerable<IPublishedContent> GetPromotedContent()
        {
            var container = _umbracoHelper.TypedContent(1234);

            #region Uncomment to see answer
            //if (container == null)
            //{
            //    return new List<IPublishedContent>();
            //}
            #endregion

            var widgets = container.Children;

            return widgets;
        }

    }
}
