﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using UnitTestingWorkshop.BusinessLogic.Services;
using UnitTestingWorkshop.Web.Models;

namespace UnitTestingWorkshop.Web.Controllers.Forms
{
    public class AccountController : SurfaceController
    {
        public AccountController(AccountService accountService)
        {
        }

        public AccountController(AccountService accountService, UmbracoContext umbracoContext) : base(umbracoContext)
        {
        }

        public ActionResult CreateMember(CreateMemberModel model)
        {
            return RedirectToCurrentUmbracoPage(new NameValueCollection { {"result", "success"} });
        }
    }
}