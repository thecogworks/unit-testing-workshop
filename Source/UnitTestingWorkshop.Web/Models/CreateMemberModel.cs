﻿namespace UnitTestingWorkshop.Web.Models
{
    public class CreateMemberModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}