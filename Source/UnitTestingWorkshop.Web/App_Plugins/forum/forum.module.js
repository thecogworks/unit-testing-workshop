﻿(function() {

    angular.module("forum", []);

    // Reference forum in umbraco app
    // Swallow exception if umbraco is not loaded
    try {
        angular.module("umbraco").requires.push("forum");
    } catch (e) {
    }


}());