﻿/* global angular */

(function () {
    
    var directive = {
        restrict: "E",
        replace: true,
        templateUrl: "/App_Plugins/forum/comment.count.directive.html",
        controller: "comment.count.controller"

        // TODO: UI "linking"
    }

    angular.module("forum").directive("commentCount", function() { return directive; });

}());